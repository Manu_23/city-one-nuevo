<?php

namespace App\Repositories;

use App\Models\Vehicles;
use App\Repositories\BaseRepository;

/**
 * Class VehiclesRepository
 * @package App\Repositories
 * @version August 12, 2020, 12:57 am UTC
*/

class VehiclesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'role'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vehicles::class;
    }
}
