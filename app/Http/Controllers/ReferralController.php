<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReferralController extends Controller
{
    public function myTree(){
        //Auth::loginUsingId(2);
        $query = Auth::user()->investment()->with('referral')->get();
        //dd($query);
        //return $query;
        $array =[];
        $array['name'] = Auth::user()->name;
        $children=[];


        foreach ($query as $q){
            $children[]= array(
                'name' => $q->name,
                'children' => $q->referral
            );
        }
        $array['children'] = $children;
        return $array;
    }


    public function allTree($id){

        $user = User::findOrFail($id);

        $query = $user->child()->get();
        //dd($query);
        //return $query;
        $array =[];
        $array['name'] = $user->name;
        $children=[];


        foreach ($query as $q){
            $children[]= array(
                'name' => $q->name,
                'children' => $q->child //
            );
        }
        $array['children'] = $children;
        return $array;
    }
}
