<?php

namespace App\Http\Controllers\API\trip;

use App\Repositories\TripRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

class TripController extends Controller
{

    private $tripRepository;

    public function __construct(TripRepository $tripRepository)
    {
        $this->tripRepository = $tripRepository;
    }


    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->tripRepository->pushCriteria(new RequestCriteria($request));
            $this->tripRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            Flash::error($e->getMessage());
        }
        $trips = $this->tripRepository->all();

        return $this->sendResponse($trips->toArray(), 'Viajes recuperados con éxito');
    }


    /**
     * Display the specified Favorite.
     * GET|HEAD /trips/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var trip $trip */
        if (!empty($this->tripRepository)) {
            $trip = $this->tripRepository->findWithoutFail($id);
        }

        if (empty($trip)) {
            return $this->sendError('Viaje no funciona');
        }

        return $this->sendResponse($trip->toArray(), 'Viaje recuperado con éxito');
    }


    /**
     * Store a newly created trip in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $trip = $this->tripRepository->create($input);
        } catch (ValidatorException $e) {
            return $this->sendError('No se pudo crear el viaje');
        }

        return $this->sendResponse($trip->toArray(), 'Viaje creado.');
    }

    /**
     * Update the specified Trip in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $trip = $this->tripRepository->findWithoutFail($id);

        if (empty($trip)) {
            return $this->sendError('trip not found');
        }
        $input = $request->all();
        $customFields = $this->tripRepository->findByField('custom_field_model', $this->tripRepository->model());
        try {
            $trip = $this->tripRepository->update($input, $id);
            $input['options'] = isset($input['options']) ? $input['options'] : [];

            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $trip->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($trip->toArray(),'Viaje ha sido actualizado');

    }

    /**
     * Remove the specified Trip from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $favorite = $this->tripRepository->findWithoutFail($id);

        if (empty($favorite)) {
            return $this->sendError('Favorite not found');

        }

        $this->tripRepository->delete($id);

        return $this->sendResponse($favorite, 'Viaje eliminado' );

    }


    public function getTrips(Request $request){

    }


}
