<?php

namespace App\Http\Controllers\API;

use App\Criteria\Categories\CategoriesOfFieldsCriteria;
use App\Criteria\Products\NearCriteria;
use App\Http\Requests\API\CreateVehiclesAPIRequest;
use App\Http\Requests\API\UpdateVehiclesAPIRequest;
use App\Models\Vehicles;
use App\Repositories\VehiclesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Response;

/**
 * Class VehiclesController
 * @package App\Http\Controllers\API
 */

class VehiclesAPIController extends AppBaseController
{
    /** @var  VehiclesRepository */
    private $vehiclesRepository;

    public function __construct(VehiclesRepository $vehiclesRepo)
    {
        $this->vehiclesRepository = $vehiclesRepo;
    }

    /**
     * Display a listing of the Vehicles.
     * GET|HEAD /vehicles
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->vehiclesRepository->pushCriteria(new RequestCriteria($request));
            $this->vehiclesRepository->pushCriteria(new LimitOffsetCriteria($request));

            // $this->vehiclesRepository->pushCriteria(new CategoriesOfFieldsCriteria($request));
        } catch (RepositoryException $e) {
            Flash::error($e->getMessage());
        }

        $categories = $this->vehiclesRepository->all();

        return $this->sendResponse($categories->toArray(), 'Vehicles retrieved successfully');
    }

    /**
     * Store a newly created Vehicles in storage.
     * POST /vehicles
     *
     * @param CreateVehiclesAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateVehiclesAPIRequest $request)
    {
        $input = $request->all();

        $vehicles = $this->vehiclesRepository->create($input);

        return $this->sendResponse($vehicles->toArray(), 'Vehicles saved successfully');
    }

    /**
     * Display the specified Vehicles.
     * GET|HEAD /vehicles/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        /** @var Vehicles $vehicles */
        $vehicles = $this->vehiclesRepository->find($id);

        if (empty($vehicles)) {
            return $this->sendError('Vehicles not found');
        }

        return $this->sendResponse($vehicles->toArray(), 'Vehicles retrieved successfully');
    }

    /**
     * Update the specified Vehicles in storage.
     * PUT/PATCH /vehicles/{id}
     *
     * @param int $id
     * @param UpdateVehiclesAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($id, UpdateVehiclesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Vehicles $vehicles */
        $vehicles = $this->vehiclesRepository->find($id);

        if (empty($vehicles)) {
            return $this->sendError('Vehicles not found');
        }

        $vehicles = $this->vehiclesRepository->update($input, $id);

        return $this->sendResponse($vehicles->toArray(), 'Vehicles updated successfully');
    }

    /**
     * Remove the specified Vehicles from storage.
     * DELETE /vehicles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        /** @var Vehicles $vehicles */
        $vehicles = $this->vehiclesRepository->find($id);

        if (empty($vehicles)) {
            return $this->sendError('Vehicles not found');
        }

        $vehicles->delete();

        return $this->sendSuccess('Vehicles deleted successfully');
    }
}
