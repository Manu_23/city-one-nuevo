<?php

namespace App\Http\Controllers;

use App\DataTables\VehiclesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVehiclesRequest;
use App\Http\Requests\UpdateVehiclesRequest;
use App\Repositories\UploadRepository;
use App\Repositories\VehiclesRepository;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Response;

class VehiclesController extends Controller
{
    /** @var  VehiclesRepository */
    private $vehiclesRepository;

    private $uploadRepository;


    public function __construct(VehiclesRepository $vehiclesRepo,  UploadRepository $uploadRepo)
    {
        parent::__construct();
        $this->vehiclesRepository = $vehiclesRepo;
        $this->uploadRepository = $uploadRepo;

    }

    /**
     * Display a listing of the Vehicles.
     *
     * @param VehiclesDataTable $vehiclesDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(VehiclesDataTable $vehiclesDataTable)
    {
        return $vehiclesDataTable->render('vehicles.index');
    }

    /**
     * Show the form for creating a new Vehicles.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('vehicles.create');
    }

    /**
     * Store a newly created Vehicles in storage.
     *
     * @param CreateVehiclesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateVehiclesRequest $request)
    {
        $input = $request->all();

        $vehicles = $this->vehiclesRepository->create($input);

        if(isset($input['image']) && $input['image']) {
            $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
            $mediaItem = $cacheUpload->getMedia('image')->first();
            $mediaItem->copy($vehicles, 'image');
        }

        Flash::success('Vehicles saved successfully.');

        return redirect(route('vehicles.index'));
    }

    /**
     * Display the specified Vehicles.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $vehicles = $this->vehiclesRepository->findWithoutFail($id);

        if (empty($vehicles)) {
            Flash::error('Vehicles not found');

            return redirect(route('vehicles.index'));
        }

        return view('vehicles.show')->with('vehicles', $vehicles);
    }

    /**
     * Show the form for editing the specified Vehicles.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $vehicles = $this->vehiclesRepository->findWithoutFail($id);

        if (empty($vehicles)) {
            Flash::error('Vehicles not found');

            return redirect(route('vehicles.index'));
        }

        return view('vehicles.edit')->with('vehicles', $vehicles);
    }

    /**
     * Update the specified Vehicles in storage.
     *
     * @param  int              $id
     * @param UpdateVehiclesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateVehiclesRequest $request)
    {
        $vehicles = $this->vehiclesRepository->findWithoutFail($id);
        $input = $request->all();

        if (empty($vehicles)) {
            Flash::error('Vehicles not found');

            return redirect(route('vehicles.index'));
        }

        $vehicles = $this->vehiclesRepository->update($request->all(), $id);

        if(isset($input['image']) && $input['image']){
            $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
            $mediaItem = $cacheUpload->getMedia('image')->first();
            $mediaItem->copy($vehicles, 'image');
        }

        Flash::success('Vehicles updated successfully.');

        return redirect(route('vehicles.index'));
    }

    /**
     * Remove the specified Vehicles from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $vehicles = $this->vehiclesRepository->findWithoutFail($id);

        if (empty($vehicles)) {
            Flash::error('Vehicles not found');

            return redirect(route('vehicles.index'));
        }

        $this->vehiclesRepository->delete($id);

        Flash::success('Vehicles deleted successfully.');

        return redirect(route('vehicles.index'));
    }


    /**
     * Remove Media of Category
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $category = $this->vehiclesRepository->findWithoutFail($input['id']);
        try {
            if($category->hasMedia($input['collection'])){
                $category->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
