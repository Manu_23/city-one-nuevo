
require('./bootstrap');

window.Vue = require('vue');



Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('referrals-component', require('./components/ReferralsAdmin.vue'));

const app = new Vue({
    el: '#app'
});
