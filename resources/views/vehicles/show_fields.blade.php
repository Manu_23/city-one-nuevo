<!-- Name Field -->
<div class="form-group  row col-6">
    {!! Form::label('name', 'Name:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{{ $vehicles->name }}</p>
</div>

<!-- Image Field -->
<div class="form-group  row col-6">
    {!! Form::label('image', 'Image:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{!! $vehicles->image !!}</p>
</div>

<!-- Role Field -->
<div class="form-group  row col-6">
    {!! Form::label('role', 'Role:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{{ $vehicles->role }}</p>
</div>

<!-- Desc Field -->
<div class="form-group  row col-6">
    {!! Form::label('desc', 'Desc:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{!! $vehicles->desc !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group  row col-6">
    {!! Form::label('created_at', 'Created At:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{{ $vehicles->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group  row col-6">
    {!! Form::label('updated_at', 'Updated At:' ,['class' => 'col-3 control-label text-right']) !!}
    <p>{{ $vehicles->updated_at }}</p>
</div>

