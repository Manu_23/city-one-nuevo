<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('image', 'Imagen', ['class' => 'col-3 control-label text-right']) !!}
    <div >
        <div style="width: 100%" class="dropzone image" id="image" data-field="image">
            <input type="hidden" name="image">
        </div>
        <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
        <div class="form-text text-muted w-50">
            sube la imagen que quieres mostrar en la app
        </div>
    </div>
</div>
@prepend('scripts')
    <script type="text/javascript">
        var var15671147011688676454ble = '';
        @if(isset($vehicles) && $vehicles->hasMedia('image'))
            var15671147011688676454ble = {
            name: "{!! $vehicles->getFirstMedia('image')->name !!}",
            size: "{!! $vehicles->getFirstMedia('image')->size !!}",
            type: "{!! $vehicles->getFirstMedia('image')->mime_type !!}",
            collection_name: "{!! $vehicles->getFirstMedia('image')->collection_name !!}"
        };
                @endif
        var dz_var15671147011688676454ble = $(".dropzone.image").dropzone({
                url: "{!!url('uploads/store')!!}",
                addRemoveLinks: true,
                maxFiles: 1,
                init: function () {
                    @if(isset($vehicles) && $vehicles->hasMedia('image'))
                    dzInit(this, var15671147011688676454ble, '{!! url($vehicles->getFirstMediaUrl('image','thumb')) !!}')
                    @endif
                },
                accept: function (file, done) {
                    dzAccept(file, done, this.element, "{!!config('medialibrary.icons_folder')!!}");
                },
                sending: function (file, xhr, formData) {
                    dzSending(this, file, formData, '{!! csrf_token() !!}');
                },
                maxfilesexceeded: function (file) {
                    dz_var15671147011688676454ble[0].mockFile = '';
                    dzMaxfile(this, file);
                },
                complete: function (file) {
                    dzComplete(this, file, var15671147011688676454ble, dz_var15671147011688676454ble[0].mockFile);
                    dz_var15671147011688676454ble[0].mockFile = file;
                },
                removedfile: function (file) {
                    dzRemoveFile(
                        file, var15671147011688676454ble, '{!! url("vehicles/remove-media") !!}',
                        'image', '{!! isset($vehicles) ? $vehicles->id : 0 !!}', '{!! url("uplaods/clear") !!}', '{!! csrf_token() !!}'
                    );
                }
            });
        dz_var15671147011688676454ble[0].mockFile = var15671147011688676454ble;
        dropzoneFields['image'] = dz_var15671147011688676454ble;
    </script>
@endprepend

<div class="clearfix"></div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', 'Role:') !!}
    {!! Form::select('role', ['uber' => 'Conductor', 'driver' => 'Repartidor', 'messenger' => 'Mensajero'], null, ['class' => 'form-control']) !!}
</div>


<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Valor:') !!}
    {!! Form::number('price', null, ['class' => 'form-control', 'placeholder', 'step'=>"any", 'min'=>"0"]) !!}
</div>


<!-- Desc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('desc', 'Desc:') !!}
    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('vehicles.index') }}" class="btn btn-default">Cancel</a>
</div>
