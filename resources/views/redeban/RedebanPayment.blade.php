<!DOCTYPE html>
<html lang="{{setting('language','es')}}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title>{{setting('app_name')}} | {{setting('app_short_description')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/png" href="{{$app_logo}}"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://cdn.globalpay.com.co/ccapi/sdk/payment_checkout_2.0.0.min.js" charset="UTF-8"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>

<body style="height: 100%; background-color: #f9f9f9;" class="vh-100">

<div id="app" class="container vh-100">
    <div class="vh-100" style="position: relative; top: 50%">
        <div>{{ $user->email }}</div>
        <div>{{ $cart['invoice_description'] }}</div>
        <br>
        <div>{{ $cart['total'] }}</div>
        <br>
        <button v-on:click="openModal()" class="btn btn-success btn-block text-uppercase">Pagar</button>
    </div>
</div>

<script>


    const INVOICE_ID = @json($cart['invoice_id']);
    const INVOICE_DES = @json($cart['invoice_description']);
    const TOTAL = {{ $cart['total']  }};
    const USER_ID = {{ $user->id  }};
    const USER = @json($user);
    const deliveryId ={{$delivery_id}};

    const url ="{{ asset('/') }}";

    const app = new Vue({
        el: '#app',
        data(){
            return{
                paymentCheckout: null,
                total: TOTAL,
                user_id: USER_ID,
                user: USER,
                invoice_id: INVOICE_ID,
                invoice_des: INVOICE_DES,
                my_url: url,
                address_id: deliveryId

            }
        },
        name:'payment-vue',
        methods:{
            openModal(){
                console.log('redeban');
                this.paymentCheckout.open({
                    user_id: this.user_id.toString(),
                    user_email: this.user.email, // Opcional
                    user_phone:  '', // Opcional
                    order_description: this.invoice_des,
                    order_amount: this.total,
                    order_vat: 0,
                    order_reference: this.invoice_id,
                });
            }
        },
        created(){
            console.log(url);
            this.paymentCheckout = new PaymentCheckout.modal({
                client_app_code: 'WINGROUP-GLP-CLIENT', // Application Code de las credenciales CLIENT
                client_app_key: 'DhhtyQUW6u1nHpX840abu5GuCOgCIl', // Application Key de las credenciales CLIENT
                locale: 'es', // Idioma preferido del usuario (es, en, pt). El inglés se usará por defecto
                env_mode: 'stg', // `prod`, `stg`, `local` para cambiar de ambiente. Por defecto es `stg`
                onOpen: function () {
                    console.log('Modal abierto');
                },
                onClose: function () {
                    console.log('Modal cerrado');
                },
                onResponse: function (response) { // Funcionalidad a invocar cuando se completa el proceso de pago
                    console.log(response.transaction.status_detail);
                    if(response.transaction.status_detail == 3){
                        axios.post(url + 'api/payments/redeban/express-checkout-success', {
                            user_id: USER_ID,
                            delivery_address_id: deliveryId,
                        }).then(res => window.location.assign(url + 'payments/redeban'));
                    }else{
                        window.location.assign(url + 'payments/redeban/error');
                    }

                    /*
                      En caso de error, esta será la respuesta.
                      response = {
                        "error": {
                          "type": "Server Error",
                          "help": "Try Again Later",
                          "description": "Sorry, there was a problem loading Checkout."
                        }
                      }

                      Cual el usuario completa el flujo en el Checkout, esta será la respuesta
                      response = {
                        "transaction":{
                            "status":"success", // Estado de la transacción
                            "id":"CB-81011", // Id de la transacción de lado de la pasarela
                            "status_detail":3 // Para más detalles de los detalles de estado: https://developers.globalpay.com.co/api/#detalle-de-los-estados
                        }
                      }
                    */
                    console.log('Respuesta de modal', response);
                    // document.getElementById('response').innerHTML = JSON.stringify(response);
                }
            });
        }
    });


</script>


</body>
</html>
